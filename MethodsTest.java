public class MethodsTest {
  public static void main(String[] args){
    int x = 5;
    System.out.println(x);
    methodNoInputNoReturn();
    System.out.println(x);
	
	System.out.println(x);
    methodOneInputNoReturn(x+10);
    System.out.println(x);

	methodTwoInputNoReturn(3, 4.5);
	
	int y = methodNoInputReturnInt();
	System.out.println(y);
	
	double z = sumSquareRoot(9, 5);
    System.out.println(z);

	String s1 = "java";
	String s2 = "programming";
	
	System.out.println(s1.length());
	System.out.println(s2.length());
	
	int testAddOne = SecondClass.addOne(50);
		System.out.println(testAddOne);
		
	SecondClass sc = new SecondClass();
	int testAddTwo = sc.addTwo(50);
		System.out.println(testAddTwo);
	
  }
  
  
  public static void methodNoInputNoReturn(){
    System.out.println("Im in a method that takes no input and returns nothing");
    int x = 20;
    System.out.println(x);
  }
  public static void methodOneInputNoReturn(int number){
	number = number -5;
    System.out.println("Inside the method one input no return");   
    System.out.println(number);
  }
  public static void methodTwoInputNoReturn(int numberInt, double numberDouble){
	  System.out.println(numberInt);
	  System.out.println(numberDouble);
  }
  public static int methodNoInputReturnInt(){
	  return 5;
  }
  public static double sumSquareRoot(int x, int y){
	  int sum = x + y;
	  double squareRoot = Math.sqrt(sum);
	  return squareRoot;
  }
}