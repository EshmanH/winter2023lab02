import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Calculator calc = new Calculator();
		
		System.out.println("Enter first number");
		double x = sc.nextDouble();
		System.out.println("Enter second number");
		double y = sc.nextDouble();
		
		double addition = Calculator.add(x, y);
		double subtraction = Calculator.subtract(x, y);
		double multiplication = calc.multiply(x,y);
		double division = calc.divide(x,y);
		
		System.out.println(addition);
		System.out.println(subtraction);
		System.out.println(multiplication);
		System.out.println(division);
	}
}